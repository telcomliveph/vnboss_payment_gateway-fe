const { Cookies } = require('quasar');

export function userAuthenticate (state,token) {
  Cookies.set('ac_user', token,{expires: '1h 0m 0s'});
}

export function finishSession(state) {
  Cookies.remove('ac_user');
}

export function checkAuthorization(state) {
  const accessToken = Cookies.get("ac_user");
  if(accessToken){
    return true
  }else{
    return false
  }
}




