export function userIsAuthorized(state){
  return state.userIsAuthorized;
}

export function userAccessToken(state) {
  return state.userAccessToken;
}