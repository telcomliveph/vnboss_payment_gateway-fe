import Vue from 'vue';
import Vuex from "vuex";
import VueRouter from 'vue-router'
import { Cookies } from "quasar";
import routes from './routes'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function ( { store } /* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  // console.log(Store);
  Router.beforeEach((to, from, next) => {
    const userToken = Cookies.get('ac_user');
    const routerAuthCheck = !userToken ? false : true
    // console.log(routerAuthCheck, "routerAuthCheck");
    
   
    if (to.matched.some(record => record.meta.requiresAuth)) {
      if(routerAuthCheck){
        //user Authenticated
        store.commit("admin/setUserIsAuthenticated", true);
        store.commit("admin/setUserAccessToken", userToken);
        next()
      }else{
        
        // Router.replace("/admin/login").catch(err => {});
      }
      
    } else {
      next() // make sure to always call next()!
    }
  })
    

  return Router
}
