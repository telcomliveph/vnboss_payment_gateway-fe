
const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "",
       component: () => import("pages/Index.vue") 
      }
    ,
    {
      path: "/register",
      component: () => import("pages/Register.vue")
    },]
  },
  
  {
    path: "/admin/login",
    component: () => import("pages/admin/Login.vue")
  },
  
  {
    path: "/admin",
    component: () => import("layouts/AdminLayout.vue"),
    meta: {
      // requiresAuth: true
    },
    children: [
      {
        path: "",
        component: () => import("pages/admin/Dashboard.vue"),
        // meta: {
        //   // requiresAuth: true
        // }
      },
      {
        path: "/admin/pay-partner/add",
        component: () => import("pages/admin/AddPayPartner.vue"),

          // meta: {
          //   // requiresAuth: true
          // }
      },
      {
        path: "/admin/agency-partner/add",
        component: () => import("pages/admin/AddAgencyPartner.vue"),

          // meta: {
          //   // requiresAuth: true
          // }
      },
      {
        path: "/admin/manage/profile",
        component: () => import("pages/admin/ManageProfile.vue"),

          // meta: {
          //   // requiresAuth: true
          // }
      },
      
      
    ]
  },
 
  
  {
    path: "*",
    component: () => import("pages/Error404.vue")
  }
];

export default routes
