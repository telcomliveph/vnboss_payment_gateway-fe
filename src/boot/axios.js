import Vue from 'vue'
import axios from 'axios'
import qs from "qs";

const API_URL = process.env.API_URL;
const axiosInstance = axios.create({
  paramsSerializer: params => qs.stringify(params, { arrayFormat: "repeat" }),
  headers: { Accept: "application/json" },
  baseURL: `${API_URL}/api`
});
console.log(process.env.API_URL,"boot");

Vue.prototype.$axios = axiosInstance;

export { axiosInstance };

